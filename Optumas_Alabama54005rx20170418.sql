#changes




CREATE TABLE "assign_1" (
  "member_id" varchar(50) DEFAULT NULL,
  "LEVEL_1" varchar(73) DEFAULT NULL,
  "LEVEL_2" varchar(73) DEFAULT NULL,
  "LEVEL_3" varchar(73) DEFAULT NULL,
  "LEVEL_4" varchar(73) DEFAULT NULL,
  "LEVEL_5" varchar(73) DEFAULT NULL,
  "claim_source" varchar(12) DEFAULT NULL,
  "assigned_type" varchar(2) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL
) ;




CREATE TABLE "Assign_PAC_Totals" (
  "EPISODE" varchar(73) DEFAULT NULL,
  "Level_Assignment" varchar(1) NOT NULL DEFAULT '',
  "IP_PAC_Count" int  NOT NULL DEFAULT '0',
  "OP_PAC_Count" int  NOT NULL DEFAULT '0',
  "PB_PAC_Count" int  NOT NULL DEFAULT '0',
  "RX_PAC_Count" int  NOT NULL DEFAULT '0' 
) ;




CREATE TABLE "assignment" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "claim_source" varchar(12) DEFAULT NULL,
  "assigned_type" varchar(2) DEFAULT NULL,
  "assigned_count" int  DEFAULT NULL,
  "rule" varchar(7) DEFAULT NULL,
  "pac" tinyint  DEFAULT '0',
  "pac_type" varchar(2) DEFAULT NULL,
  "episode_period" tinyint  DEFAULT NULL,
  "IP_period" tinyint  DEFAULT NULL 
) ;




CREATE TABLE "association" (
  "id" int   NOT NULL ,
  "parent_master_episode_id" varchar(73) DEFAULT NULL,
  "child_master_episode_id" varchar(73) DEFAULT NULL,
  "association_type" varchar(24) DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
  "association_count" int  DEFAULT NULL,
  "association_start_day" varchar(15) DEFAULT NULL,
  "association_end_day" varchar(15) DEFAULT NULL 
) ;




CREATE TABLE "build_episode_reference" (
  "EPISODE_ID" varchar(6) NOT NULL,
  "NAME" varchar(6) DEFAULT NULL,
  "TYPE" varchar(45) NOT NULL,
  "STATUS" varchar(45) DEFAULT NULL,
  "DESCRIPTION" varchar(37) DEFAULT NULL,
  "CREATED_DATE" datetime NOT NULL,
  "MODIFIED_DATE" datetime DEFAULT NULL,
  "USERS_USER_ID" varchar(45) DEFAULT NULL,
  "MDC_CATEGORY" varchar(2) NOT NULL,
  "PARM_SET" int  DEFAULT '1',
  "TRIGGER_TYPE" int  DEFAULT NULL,
  "TRIGGER_NUMBER" int  DEFAULT NULL,
  "SEPARATION_MIN" int  DEFAULT NULL,
  "SEPARATION_MAX" int  DEFAULT NULL,
  "BOUND_OFFSET" int  DEFAULT NULL,
  "BOUND_LENGTH" int  DEFAULT NULL,
  "CONDITION_MIN" int  DEFAULT NULL,
  "VERSION" varchar(45) DEFAULT NULL,
  "END_OF_STUDY" tinyint  DEFAULT NULL,
) ;




CREATE TABLE "build_episode_reference_old" (
  "EPISODE_ID" varchar(6)  NOT NULL,
  "NAME" varchar(6)  DEFAULT NULL,
  "TYPE" varchar(45)  NOT NULL,
  "STATUS" varchar(45)  DEFAULT NULL,
  "DESCRIPTION" varchar(37)  DEFAULT NULL,
  "CREATED_DATE" datetime NOT NULL,
  "MODIFIED_DATE" datetime DEFAULT NULL,
  "USERS_USER_ID" varchar(45)  DEFAULT NULL,
  "MDC_CATEGORY" varchar(2)  NOT NULL,
  "PARM_SET" int  DEFAULT '1',
  "TRIGGER_TYPE" int  DEFAULT NULL,
  "TRIGGER_NUMBER" int  DEFAULT NULL,
  "SEPARATION_MIN" int  DEFAULT NULL,
  "SEPARATION_MAX" int  DEFAULT NULL,
  "BOUND_OFFSET" int  DEFAULT NULL,
  "BOUND_LENGTH" int  DEFAULT NULL,
  "CONDITION_MIN" int  DEFAULT NULL,
  "VERSION" varchar(45)  DEFAULT NULL,
  "END_OF_STUDY" tinyint  DEFAULT NULL
) ;




CREATE TABLE "chronic_core_categories" (
  "episode_id" varchar(80)  NOT NULL DEFAULT '',
  "Match_to" varchar(1) NOT NULL DEFAULT '',
  "Code Groups" char(11)  NOT NULL DEFAULT '',
  "category" varchar(80)  DEFAULT NULL,
  "Core_Count" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "CHRONIC_CORE_CLMS_CNT_2" (
  "episode_id" varchar(80)  NOT NULL DEFAULT '',
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "attrib_default_physician" varchar(30) DEFAULT NULL,
  "attrib_default_facility" varchar(30) DEFAULT NULL,
  "category" varchar(80)  DEFAULT NULL,
  "core_count" double precision precision precision DEFAULT NULL,
  "Catergory_Costs_365_back" numeric(45,0) DEFAULT NULL,
  "Catergory_Costs_365_forward" numeric(45,0) DEFAULT NULL,
  "PT_CAT_COUNT_365_back" numeric(45,0) DEFAULT NULL,
  "PT_CAT_COUNT_365_forward" numeric(45,0) DEFAULT NULL,
  "PT_CAT_USE_END" varchar(5) DEFAULT NULL,
  "PT_CAT_USE_START" varchar(5) DEFAULT NULL
) ;




CREATE TABLE "CHRONIC_CORE_CLMS_flag" (
  "episode_id" varchar(6) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "Cost" numeric(44,24) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "attrib_default_physician" varchar(30) DEFAULT NULL,
  "attrib_default_facility" varchar(30) DEFAULT NULL,
  "begin_Date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "Core Service Category" varchar(80)  DEFAULT NULL,
  "Core_Count" double precision precision precision DEFAULT NULL,
  "DAY_FROM_EPI_END" int  DEFAULT NULL,
  "DAY_FROM_EPI_START" int  DEFAULT NULL
) ;




CREATE TABLE "claim_line" (
  "id" int   NOT NULL ,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "claim_id" varchar(50) DEFAULT NULL,
  "claim_line_id" varchar(22) DEFAULT NULL,
  "sequence_key" varchar(11) DEFAULT NULL,
  "final_version_flag" varchar(2) DEFAULT NULL,
  "claim_encounter_flag" tinyint  DEFAULT NULL,
  "provider_npi" varchar(12) DEFAULT NULL,
  "provider_id" varchar(24) DEFAULT NULL,
  "physician_id" varchar(24) DEFAULT NULL,
  "facility_id" varchar(24) DEFAULT NULL,
  "allowed_amt" numeric(40,20) DEFAULT NULL,
  "real_allowed_amt" numeric(40,20) DEFAULT NULL,
  "proxy_allowed_amt" numeric(40,20) DEFAULT NULL,
  "facility_type_code" varchar(8) DEFAULT NULL,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "place_of_svc_code" varchar(25) DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "assigned" tinyint  DEFAULT '0',
  "assigned_count" tinyint  DEFAULT '0',
  "quantity" int  DEFAULT NULL,
  "standard_payment_amt" numeric(40,20) DEFAULT NULL,
  "charge_amt" numeric(40,20) DEFAULT NULL,
  "paid_amt" numeric(15,4) DEFAULT NULL,
  "prepaid_amt" numeric(15,4) DEFAULT NULL,
  "copay_amt" numeric(15,4) DEFAULT NULL,
  "coinsurance_amt" numeric(15,4) DEFAULT NULL,
  "deductible_amt" numeric(15,4) DEFAULT NULL,
  "insurance_product" varchar(24) DEFAULT NULL,
  "plan_id" varchar(24) DEFAULT NULL,
  "admission_date" date DEFAULT NULL,
  "admission_src_code" varchar(12) DEFAULT NULL,
  "admit_type_code" varchar(12) DEFAULT NULL,
  "discharge_status_code" varchar(12) DEFAULT NULL,
  "discharge_date" date DEFAULT NULL,
  "type_of_bill" varchar(12) DEFAULT NULL,
  "rev_count" int  DEFAULT NULL,
  "drg_version" varchar(12) DEFAULT NULL,
  "ms_drg_code" varchar(12) DEFAULT NULL,
  "apr_drg_code" varchar(12) DEFAULT NULL,
  "readmission" tinyint  DEFAULT '0',
  "office_visit" tinyint  DEFAULT '0',
  "trigger" tinyint  DEFAULT '0',
  "ed_visit" tinyint  DEFAULT '0',
  "ed_visit_id" varchar(12) DEFAULT NULL,
  "core_service" tinyint  DEFAULT '0',
  "pas" tinyint  DEFAULT '0',
) ;




CREATE TABLE "claim_line_rx" (
  "id" int   NOT NULL ,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "claim_id" varchar(50) DEFAULT NULL,
  "sequence_key" varchar(11) DEFAULT NULL,
  "final_version_flag" varchar(5) DEFAULT NULL,
  "claim_encounter_flag" tinyint  DEFAULT NULL,
  "allowed_amt" numeric(40,20) DEFAULT NULL,
  "real_allowed_amt" numeric(40,20) DEFAULT NULL,
  "proxy_allowed_amt" numeric(40,20) DEFAULT NULL,
  "assigned" tinyint  DEFAULT '0',
  "assigned_count" tinyint  DEFAULT NULL,
  "line_counter" int  DEFAULT NULL,
  "charge_amt" numeric(40,20) DEFAULT NULL,
  "paid_amt" numeric(40,20) DEFAULT NULL,
  "prepaid_amt" numeric(40,20) DEFAULT NULL,
  "copay_amt" numeric(40,20) DEFAULT NULL,
  "coinsurance_amt" numeric(40,20) DEFAULT NULL,
  "deductible_amt" numeric(40,20) DEFAULT NULL,
  "drug_nomen" varchar(10) DEFAULT NULL,
  "drug_code" varchar(25) DEFAULT NULL,
  "drug_name" varchar(50) DEFAULT NULL,
  "builder_match_code" varchar(50) DEFAULT NULL,
  "days_supply_amt" int  DEFAULT NULL,
  "quantityDispensed" numeric(40,20) DEFAULT NULL,
  "rx_fill_date" date DEFAULT NULL,
  "prescribing_provider_id" varchar(24) DEFAULT NULL,
  "prescribing_provider_npi" varchar(12) DEFAULT NULL,
  "prescribing_provider_dea" varchar(20) DEFAULT NULL,
  "pharmacy_zip_code" varchar(16) DEFAULT NULL,
  "insurance_product" varchar(24) DEFAULT NULL,
  "genericDrugIndicator" varchar(6) DEFAULT NULL,
  "national_pharmacy_Id" varchar(20) DEFAULT NULL,
  "orig_adj_rev" varchar(12) DEFAULT NULL,
  "plan_id" varchar(24) DEFAULT NULL 
) ;




CREATE TABLE "claims_combined" (
  "id" int   NOT NULL ,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "allowed_amt" numeric(40,20) DEFAULT NULL,
  "assigned_count" tinyint  DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL 
) ;




CREATE TABLE "code" (
  "id" int   NOT NULL ,
  "u_c_id" int   NOT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "function_code" varchar(10) DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "nomen" varchar(7) DEFAULT NULL,
  "principal" tinyint  DEFAULT '0',
) ;




CREATE TABLE "Descriptive_Stats" (
  "Field" varchar(100) DEFAULT NULL,
  "Data" varchar(100) DEFAULT NULL
) ;




CREATE TABLE "DYO_SAVINGS_BY_EPI" (
  "filter_id" int  DEFAULT NULL,
  "episode_id" varchar(6) DEFAULT NULL,
  "episode_name" varchar(6) DEFAULT NULL,
  "episode_type" varchar(50) DEFAULT NULL,
  "level" int  DEFAULT NULL,
  "episode_volume" int  NOT NULL DEFAULT '0',
  "Total_Potential_PAC_Savings" numeric(39,6) DEFAULT NULL,
  "Total_Potential_Typical_Savings" numeric(36,2) DEFAULT NULL
) ;




CREATE TABLE "enrollment" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "age_at_enrollment" int  DEFAULT NULL,
  "insurance_product" varchar(24) DEFAULT NULL,
  "coverage_type" varchar(24) DEFAULT NULL,
  "isGap" boolean  DEFAULT '0',
) ;




CREATE TABLE "episode" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "claim_id" varchar(50) DEFAULT NULL,
  "claim_line_id" varchar(22) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "episode_type" varchar(1) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "episode_length_days" int  DEFAULT NULL,
  "trig_begin_date" date DEFAULT NULL,
  "trig_end_date" date DEFAULT NULL,
  "associated" tinyint  DEFAULT '0',
  "association_count" int  DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
  "truncated" tinyint  DEFAULT '0',
  "attrib_default_physician" varchar(30) DEFAULT NULL,
  "attrib_default_facility" varchar(30) DEFAULT NULL 
) ;




CREATE TABLE "episode_aggregates" (
  "episode_id" varchar(10) DEFAULT NULL,
  "filter_id" int  DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "episode_count" int  DEFAULT '0',
  "total_cost_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "total_cost_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "avg_cost_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "avg_cost_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "standard_deviation_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "standard_deviation_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cv_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cv_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percent_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percent_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "typical_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "typical_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "typ_w_comp_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "typ_w_comp_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "pac_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "pac_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "complication_rate_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "complication_rate_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_1_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_80_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_99_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_1_ann_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_80_ann_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_99_ann_costs_split" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_1_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_99_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_1_ann_costs_unsplit" numeric(40,20) DEFAULT '0.00000000000000000000',
  "percentile_99_ann_costs_unsplit" numeric(40 20) DEFAULT '0.00000000000000000000' 
) ;




CREATE TABLE "episode_information" (
   "EPISODE_ID" VARCHAR(6) NOT NULL,
   "NAME" VARCHAR(6) NULL DEFAULT NULL,
   "TYPE" VARCHAR(45) NOT NULL,
   "STATUS" VARCHAR(45) NULL DEFAULT NULL,
   "DESCRIPTION" VARCHAR(37) NULL DEFAULT NULL,
   "CREATED_DATE" DATETIME NOT NULL,
   "MODIFIED_DATE" DATETIME NULL DEFAULT NULL,
   "USERS_USER_ID" VARCHAR(45) NULL DEFAULT NULL,
   "MDC_CATEGORY" VARCHAR(2) NOT NULL,
   "PARM_SET" int  NULL DEFAULT '1',
   "TRIGGER_TYPE" int  NULL DEFAULT NULL,
   "TRIGGER_NUMBER" int  NULL DEFAULT NULL,
   "SEPARATION_MIN" int  NULL DEFAULT NULL,
   "SEPARATION_MAX" int  NULL DEFAULT NULL,
   "BOUND_OFFSET" int  NULL DEFAULT NULL,
   "BOUND_LENGTH" int  NULL DEFAULT NULL,
   "CONDITION_MIN" int  NULL DEFAULT NULL,
   "VERSION" VARCHAR(45) NULL DEFAULT NULL,
   "END_OF_STUDY" int  NULL DEFAULT NULL
) ;




CREATE TABLE "episode_sub_types" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "subtype_group_id" varchar(25) DEFAULT NULL,
  "group_name" varchar(255) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
) ;




CREATE TABLE "filter_params" (
  "filter_id" int   NOT NULL,
  "episode_id" varchar(11) DEFAULT NULL,
  "lower_age_limit" int  DEFAULT NULL,
  "upper_age_limit" int  DEFAULT NULL,
) ;




CREATE TABLE "filtered_episodes" (
  "filter_id" int  DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "filter_fail" binary(1) DEFAULT '0',
  "age_limit_lower" binary(1) NOT NULL DEFAULT '0',
  "age_limit_upper" binary(1) NOT NULL DEFAULT '0',
  "episode_cost_upper" binary(1) NOT NULL DEFAULT '0',
  "episode_cost_lower" binary(1) NOT NULL DEFAULT '0',
  "coverage_gap" binary(1) NOT NULL DEFAULT '0',
  "episode_complete" binary(1) NOT NULL DEFAULT '0',
  "drg" binary(1) NOT NULL DEFAULT '0',
  "proc_ep_orphan" binary(1) NOT NULL DEFAULT '0',
  "proc_ep_orph_triggered" binary(1) NOT NULL DEFAULT '0',
) ;




CREATE TABLE "filters" (
  "filter_id" int   NOT NULL ,
  "fiilter_name" varchar(73) DEFAULT NULL,
) ;




CREATE TABLE "filters_MAT_NBORN" (
  "member_id" varchar(50) DEFAULT NULL,
  "episode_id" varchar(6) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "CSECT_WARRENTED_FILTER" int  DEFAULT NULL,
  "INDUCTION_WARRANTED_FILTER" int  DEFAULT NULL,
  "NICU_LVL_4_FILTER" int  DEFAULT NULL
) ;




CREATE TABLE "global_risk_factors" (
  "member_id" varchar(50) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "claim_date" date DEFAULT NULL,
  "type_id" varchar(3) DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "factor_id" varchar(25) DEFAULT NULL
) ;




CREATE TABLE "hibernate_sequence" (
  "next_val" int  NOT NULL 
) ;




CREATE TABLE "level_5" (
  "member_id" varchar(50) DEFAULT NULL,
  "LEVEL_1" varchar(73) DEFAULT NULL,
  "LEVEL_2" varchar(73) DEFAULT NULL,
  "LEVEL_3" varchar(73) DEFAULT NULL,
  "LEVEL_4" varchar(73) DEFAULT NULL,
  "LEVEL_5" varchar(73) DEFAULT NULL 
) ;




CREATE TABLE "master_epid_code" (
  "id" int   NOT NULL ,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "nomen" varchar(7) DEFAULT NULL,
  "pas" tinyint  DEFAULT '0',
  "core_service" tinyint  DEFAULT '0',
  "pac" tinyint  DEFAULT '0',
  "pac_type" tinyint  DEFAULT NULL,
  "risk_factor" varchar(14) DEFAULT NULL,
  "sub_type" varchar(14) DEFAULT NULL,
  "betos" varchar(24) DEFAULT NULL 
) ;




CREATE TABLE "master_epid_level" (
  "id" int   NOT NULL ,
  "filter_id" int  DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "claim_type" varchar(2) DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "split" tinyint  DEFAULT '0',
  "annualized" tinyint  DEFAULT '0',
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40,20) DEFAULT NULL,
  "risk_factor_count" int  DEFAULT NULL,
  "sub_type_count" int  DEFAULT NULL,
  "probability_of_complications" int  DEFAULT NULL,
  "IP_stay_count" int  DEFAULT NULL,
  "IP_stay_facility_costs" numeric(40,20) DEFAULT NULL,
  "IP_stay_prof_costs" numeric(40,20) DEFAULT NULL,
  "IP_stay_total_costs" numeric(40,20) DEFAULT NULL,
  "IP_stay_bed_days" int  DEFAULT NULL,
  "IP_stay_avg_length" int  DEFAULT NULL 
) ;




CREATE TABLE "mdc_desc" (
  "mdc" varchar(5) NOT NULL DEFAULT '',
  "mdc_description" varchar(255) DEFAULT NULL 
) ;




CREATE TABLE "member" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "gender_code" varchar(2) DEFAULT NULL,
  "race_code" varchar(2) DEFAULT NULL,
  "zip_code" varchar(16) DEFAULT NULL,
  "birth_year" int  DEFAULT NULL,
  "age" tinyint  DEFAULT NULL,
  "enforced_provider_id" varchar(24) DEFAULT NULL,
  "primary_care_provider_id" varchar(24) DEFAULT NULL,
  "primary_care_provider_npi" varchar(11) DEFAULT NULL,
  "pcp_effective_date" date DEFAULT NULL,
  "date_of_death" date DEFAULT NULL,
  "insurance_type" varchar(24) DEFAULT NULL,
  "insurance_carrier" varchar(24) DEFAULT NULL,
  "dual_eligible" tinyint  DEFAULT NULL,
  "months_eligible_total" int  DEFAULT NULL,
  "cost_of_care" numeric(40,20) DEFAULT NULL,
  "unassigned_cost" numeric(40,20) DEFAULT NULL,
  "assigned_cost" numeric(40,20) DEFAULT NULL,
  "ed_visits" int  DEFAULT NULL,
  "ed_cost" numeric(40,20) DEFAULT NULL,
  "ip_stay_count" int  DEFAULT NULL,
  "ip_stay_cost" numeric(40,20) DEFAULT NULL,
  "bed_days" int  DEFAULT NULL,
  "alos" int  DEFAULT NULL,
  "claim_lines" int  DEFAULT NULL,
  "claim_lines_t" int  DEFAULT NULL,
  "claim_lines_tc" int  DEFAULT NULL,
  "claim_lines_c" int  DEFAULT NULL,
  "ip_readmit_count" int  DEFAULT NULL,
  "ip_readmit_cost" numeric(40,20) DEFAULT NULL,
  "designated_pcp" varchar(24) DEFAULT NULL,
  "plan_id" varchar(24) DEFAULT NULL,
  "rf_count" int  DEFAULT NULL,
  "st_count" int  DEFAULT NULL,
  "aco_id" varchar(24) DEFAULT NULL,
  "aco_name" varchar(45) DEFAULT NULL,
) ;




CREATE TABLE "member_epid_level" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "episode_id" varchar(12) DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "split" tinyint  DEFAULT '0',
  "annualized" tinyint  DEFAULT '0',
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40,20) DEFAULT NULL,
) ;




CREATE TABLE "member_master_epid_level" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "split" tinyint  DEFAULT '0',
  "annualized" tinyint  DEFAULT '0',
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40,20) DEFAULT NULL,
) ;




CREATE TABLE "memberValidationReport" (
  "id" int  NOT NULL ,
  "IPClaimCount" int  NOT NULL,
  "IPClaimTotalAmount" numeric(19,2) DEFAULT NULL,
  "OPClaimCount" int  NOT NULL,
  "OPClaimTotalAmount" numeric(19,2) DEFAULT NULL,
  "PBClaimCount" int  NOT NULL,
  "PBClaimTotalAmount" numeric(19,2) DEFAULT NULL,
  "RXClaimTotalAmount" numeric(19,2) DEFAULT NULL,
  "RxClaimCount" int  NOT NULL,
  "authorizeDate" datetime DEFAULT NULL,
  "authorizer" varchar(255) DEFAULT NULL,
  "bypass" boolean  NOT NULL,
  "jobUid" int  NOT NULL,
  "member_id" varchar(255) DEFAULT NULL,
) ;




CREATE TABLE "ndc_to_multum" (
  "id" int   NOT NULL ,
  "multum" varchar(10) DEFAULT NULL,
  "ndc" varchar(12) DEFAULT NULL,
  "drug_category" varchar(14) DEFAULT NULL,
) ;




CREATE TABLE "PAC_DATA" (
  "episode_id" varchar(6) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "trig_begin_date" date DEFAULT NULL,
  "trig_end_date" date DEFAULT NULL,
  "attrib_default_physician" varchar(30) DEFAULT NULL,
  "attrib_default_facility" varchar(30) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "assigned_count" int  DEFAULT NULL,
  "assigned_type" varchar(2) DEFAULT NULL,
  "claim_source" varchar(12) DEFAULT NULL,
  "rule" varchar(7) DEFAULT NULL,
  "id" int   DEFAULT '0',
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "allowed_amt" numeric(40,20) DEFAULT NULL,
  "Day_from_epi_end" int  DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "nomen" varchar(7) DEFAULT NULL,
  "COMPLICATION" int  DEFAULT NULL,
  "group_id" char(11)  DEFAULT 'XX0000',
  "description" varchar(250) ,
  "group_name" varchar(80)  DEFAULT NULL
) ;




CREATE TABLE "PAS_episodes_lines_attrib_code_mel_all" (
  "episode_id" varchar(10) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "assigned_type" varchar(2) DEFAULT NULL,
  "rule" varchar(7) DEFAULT NULL,
  "assigned_count" int  DEFAULT NULL,
  "allowed_amt" numeric(40,20) DEFAULT NULL,
  "claim_provider_id" varchar(24) DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "id" int   DEFAULT NULL,
  "attrib_default_physician" varchar(30) DEFAULT NULL,
  "attrib_default_facility" varchar(30) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "Days_from_epi_end" int  DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "principal" tinyint  DEFAULT '0',
  "filter_id" int  DEFAULT NULL,
  "cost" numeric(40 20) DEFAULT NULL
) ;




CREATE TABLE "PAS_FLAG_all" (
  "PAS_flag" numeric(65,0) DEFAULT NULL,
  "PAS_costs" numeric(62,20) DEFAULT NULL,
  "Antibiotics" numeric(23,0) DEFAULT NULL,
  "Anticoagulant Management" numeric(23,0) DEFAULT NULL,
  "Anticoagulants" numeric(23,0) DEFAULT NULL,
  "Asthma inhalers" numeric(23,0) DEFAULT NULL,
  "Blood Gases, Pulse Oximetry" numeric(23,0) DEFAULT NULL,
  "Blood Transfusion" numeric(23,0) DEFAULT NULL,
  "Bone Density" numeric(23,0) DEFAULT NULL,
  "Bone Scan" numeric(23,0) DEFAULT NULL,
  "Brain EEG" numeric(23,0) DEFAULT NULL,
  "Cardiac Cath and Angiography" numeric(23,0) DEFAULT NULL,
  "Cardiac Stress Test" numeric(23,0) DEFAULT NULL,
  "Chest X-Ray" numeric(23,0) DEFAULT NULL,
  "CPAP and other Respiratory Treatments" numeric(23,0) DEFAULT NULL,
  "CT abdomen" numeric(23,0) DEFAULT NULL,
  "CT or CTA Chest" numeric(23,0) DEFAULT NULL,
  "CT or CTA Head" numeric(23,0) DEFAULT NULL,
  "CT or CTA pelvis" numeric(23,0) DEFAULT NULL,
  "CT or MRI facial area orbit post fossa" numeric(23,0) DEFAULT NULL,
  "CT soft tissues neck" numeric(23,0) DEFAULT NULL,
  "CT Spine" numeric(23,0) DEFAULT NULL,
  "DVT Management - IVC filter" numeric(23,0) DEFAULT NULL,
  "DVT Management - Ligation of IVC" numeric(23,0) DEFAULT NULL,
  "Electrocardiogram" numeric(23,0) DEFAULT NULL,
  "Heart echo" numeric(23,0) DEFAULT NULL,
  "Home visit" numeric(23,0) DEFAULT NULL,
  "Immunoglobulin Antibiodies" numeric(23,0) DEFAULT NULL,
  "Indwelling bladder catheter" numeric(23,0) DEFAULT NULL,
  "Intravascular stent, thromboendarterectomy" numeric(23,0) DEFAULT NULL,
  "Lab tests CBC Blood Chemistry" numeric(23,0) DEFAULT NULL,
  "Lipid Panel" numeric(23,0) DEFAULT NULL,
  "Lung Function Tests" numeric(23,0) DEFAULT NULL,
  "Lung Percussion" numeric(23,0) DEFAULT NULL,
  "Lung Scan" numeric(23,0) DEFAULT NULL,
  "MRI Brain" numeric(23,0) DEFAULT NULL,
  "MRI Chest" numeric(23,0) DEFAULT NULL,
  "MRI lower extremity" numeric(23,0) DEFAULT NULL,
  "MRI Spine" numeric(23,0) DEFAULT NULL,
  "MRI upper extremity" numeric(23,0) DEFAULT NULL,
  "Other Cardiac Imaging" numeric(23,0) DEFAULT NULL,
  "Pain relief" numeric(23,0) DEFAULT NULL,
  "Pap smear" numeric(23,0) DEFAULT NULL,
  "PSA Screening" numeric(23,0) DEFAULT NULL,
  "Screening Pelvic Breast" numeric(23,0) DEFAULT NULL,
  "Steroids" numeric(23,0) DEFAULT NULL,
  "Tests for allergens antibodies infectious agents" numeric(23,0) DEFAULT NULL,
  "Tumor Imaging" numeric(23,0) DEFAULT NULL,
  "Ultrasound abdomen / pelvis" numeric(23,0) DEFAULT NULL,
  "Ultrasound extracranial arteries" numeric(23,0) DEFAULT NULL,
  "Ultrasound extremities" numeric(23,0) DEFAULT NULL,
  "Venous thrombosis imaging" numeric(23,0) DEFAULT NULL,
  "X-Ray Spine" numeric(23,0) DEFAULT NULL,
  "Filter_ID" int  DEFAULT NULL,
  "Member_ID" varchar(50) DEFAULT NULL,
  "Master_Episode_ID" varchar(255) DEFAULT NULL,
  "Episode_ID" varchar(6) DEFAULT NULL,
  "Episode_Name" varchar(6) DEFAULT NULL,
  "Episode_Description" varchar(255) DEFAULT NULL,
  "Episode_Type" varchar(50) DEFAULT NULL,
  "MDC" int  DEFAULT NULL,
  "MDC_Description" varchar(255) DEFAULT NULL,
  "Episode_Begin_Date" varchar(10) DEFAULT NULL,
  "Episode_End_Date" varchar(10) DEFAULT NULL,
  "Episode_Length" int  DEFAULT NULL,
  "Level" int  DEFAULT NULL,
  "Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Facility_ID" varchar(50) DEFAULT NULL,
  "Physician_ID" varchar(50) DEFAULT NULL,
  "Physician_Specialty" varchar(2) DEFAULT NULL,
  "Split_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "IP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "OP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "PB_PAC_Count" numeric(13,2) DEFAULT NULL,
  "RX_PAC_Count" numeric(13,2) DEFAULT NULL
) ;




CREATE TABLE "PAS_provider_avg" (
  "episode_id" varchar(6) DEFAULT NULL,
  "PROVIDER" varchar(50) DEFAULT NULL,
  "level" int  DEFAULT NULL,
  "filter_id" int  DEFAULT NULL,
  "episode_count" int  NOT NULL DEFAULT '0',
  "PAS_EPISODE_COUNT" numeric(23,0) DEFAULT NULL,
  "PERCENT_PTS_W_PAS" numeric(27,4) DEFAULT NULL,
  "PAS_COSTS" numeric(65,20) DEFAULT NULL,
  "AVG_PAS_COSTS" numeric(65,24) DEFAULT NULL,
  "AVG_SPLIT_COSTS" numeric(39,6) DEFAULT NULL,
  "AVG_ANNUAL_SPLIT_COSTS" numeric(39,6) DEFAULT NULL,
  "Antibiotics" numeric(45,0) DEFAULT NULL,
  "Anticoagulant Management" numeric(45,0) DEFAULT NULL,
  "Anticoagulants" numeric(45,0) DEFAULT NULL,
  "Asthma inhalers" numeric(45,0) DEFAULT NULL,
  "Blood Gases, Pulse Oximetry" numeric(45,0) DEFAULT NULL,
  "Blood Transfusion" numeric(45,0) DEFAULT NULL,
  "Bone Density" numeric(45,0) DEFAULT NULL,
  "Bone Scan" numeric(45,0) DEFAULT NULL,
  "Brain EEG" numeric(45,0) DEFAULT NULL,
  "Cardiac Cath and Angiography" numeric(45,0) DEFAULT NULL,
  "Cardiac Stress Test" numeric(45,0) DEFAULT NULL,
  "Chest X-Ray" numeric(45,0) DEFAULT NULL,
  "CPAP and other Respiratory Treatments" numeric(45,0) DEFAULT NULL,
  "CT abdomen" numeric(45,0) DEFAULT NULL,
  "CT or CTA Chest" numeric(45,0) DEFAULT NULL,
  "CT or CTA Head" numeric(45,0) DEFAULT NULL,
  "CT or CTA pelvis" numeric(45,0) DEFAULT NULL,
  "CT or MRI facial area orbit post fossa" numeric(45,0) DEFAULT NULL,
  "CT soft tissues neck" numeric(45,0) DEFAULT NULL,
  "CT Spine" numeric(45,0) DEFAULT NULL,
  "DVT Management - IVC filter" numeric(45,0) DEFAULT NULL,
  "DVT Management - Ligation of IVC" numeric(45,0) DEFAULT NULL,
  "Electrocardiogram" numeric(45,0) DEFAULT NULL,
  "Heart echo" numeric(45,0) DEFAULT NULL,
  "Home visit" numeric(45,0) DEFAULT NULL,
  "Immunoglobulin Antibiodies" numeric(45,0) DEFAULT NULL,
  "Indwelling bladder catheter" numeric(45,0) DEFAULT NULL,
  "Intravascular stent, thromboendarterectomy" numeric(45,0) DEFAULT NULL,
  "Lab tests CBC Blood Chemistry" numeric(45,0) DEFAULT NULL,
  "Lipid Panel" numeric(45,0) DEFAULT NULL,
  "Lung Function Tests" numeric(45,0) DEFAULT NULL,
  "Lung Percussion" numeric(45,0) DEFAULT NULL,
  "Lung Scan" numeric(45,0) DEFAULT NULL,
  "MRI Brain" numeric(45,0) DEFAULT NULL,
  "MRI Chest" numeric(45,0) DEFAULT NULL,
  "MRI lower extremity" numeric(45,0) DEFAULT NULL,
  "MRI Spine" numeric(45,0) DEFAULT NULL,
  "MRI upper extremity" numeric(45,0) DEFAULT NULL,
  "Other Cardiac Imaging" numeric(45,0) DEFAULT NULL,
  "Pain relief" numeric(45,0) DEFAULT NULL,
  "Pap smear" numeric(45,0) DEFAULT NULL,
  "PSA Screening" numeric(45,0) DEFAULT NULL,
  "Screening Pelvic Breast" numeric(45,0) DEFAULT NULL,
  "Steroids" numeric(45,0) DEFAULT NULL,
  "Tests for allergens antibodies infectious agents" numeric(45,0) DEFAULT NULL,
  "Tumor Imaging" numeric(45,0) DEFAULT NULL,
  "Ultrasound abdomen / pelvis" numeric(45,0) DEFAULT NULL,
  "Ultrasound extracranial arteries" numeric(45,0) DEFAULT NULL,
  "Ultrasound extremities" numeric(45,0) DEFAULT NULL,
  "Venous thrombosis imaging" numeric(45,0) DEFAULT NULL,
  "X-Ray Spine" numeric(45,0) DEFAULT NULL,
  "AVG Antibiotics" numeric(49,4) DEFAULT NULL,
  "AVG Anticoagulant Management" numeric(49,4) DEFAULT NULL,
  "AVG Anticoagulants" numeric(49,4) DEFAULT NULL,
  "AVG Asthma inhalers" numeric(49,4) DEFAULT NULL,
  "AVG Blood Gases, Pulse Oximetry" numeric(49,4) DEFAULT NULL,
  "AVG Blood Transfusion" numeric(49,4) DEFAULT NULL,
  "AVG Bone Density" numeric(49,4) DEFAULT NULL,
  "AVG Bone Scan" numeric(49,4) DEFAULT NULL,
  "AVG Brain EEG" numeric(49,4) DEFAULT NULL,
  "AVG Cardiac Cath and Angiography" numeric(49,4) DEFAULT NULL,
  "AVG Cardiac Stress Test" numeric(49,4) DEFAULT NULL,
  "AVG Chest X-Ray" numeric(49,4) DEFAULT NULL,
  "AVG CPAP and other Respiratory Treatments" numeric(49,4) DEFAULT NULL,
  "AVG CT abdomen" numeric(49,4) DEFAULT NULL,
  "AVG CT or CTA Chest" numeric(49,4) DEFAULT NULL,
  "AVG CT or CTA Head" numeric(49,4) DEFAULT NULL,
  "AVG CT or CTA pelvis" numeric(49,4) DEFAULT NULL,
  "AVG CT or MRI facial area orbit post fossa" numeric(49,4) DEFAULT NULL,
  "AVG CT soft tissues neck" numeric(49,4) DEFAULT NULL,
  "AVG CT Spine" numeric(49,4) DEFAULT NULL,
  "AVG DVT Management - IVC filter" numeric(49,4) DEFAULT NULL,
  "AVG DVT Management - Ligation of IVC" numeric(49,4) DEFAULT NULL,
  "AVG Electrocardiogram" numeric(49,4) DEFAULT NULL,
  "AVG Heart echo" numeric(49,4) DEFAULT NULL,
  "AVG Home visit" numeric(49,4) DEFAULT NULL,
  "AVG Immunoglobulin Antibiodies" numeric(49,4) DEFAULT NULL,
  "AVG Indwelling bladder catheter" numeric(49,4) DEFAULT NULL,
  "AVG Intravascular stent, thromboendarterectomy" numeric(49,4) DEFAULT NULL,
  "AVG Lab tests CBC Blood Chemistry" numeric(49,4) DEFAULT NULL,
  "AVG Lipid Panel" numeric(49,4) DEFAULT NULL,
  "AVG Lung Function Tests" numeric(49,4) DEFAULT NULL,
  "AVG Lung Percussion" numeric(49,4) DEFAULT NULL,
  "AVG Lung Scan" numeric(49,4) DEFAULT NULL,
  "AVG MRI Brain" numeric(49,4) DEFAULT NULL,
  "AVG MRI Chest" numeric(49,4) DEFAULT NULL,
  "AVG MRI lower extremity" numeric(49,4) DEFAULT NULL,
  "AVG MRI Spine" numeric(49,4) DEFAULT NULL,
  "AVG MRI upper extremity" numeric(49,4) DEFAULT NULL,
  "AVG Other Cardiac Imaging" numeric(49,4) DEFAULT NULL,
  "AVG Pain relief" numeric(49,4) DEFAULT NULL,
  "AVG Pap smear" numeric(49,4) DEFAULT NULL,
  "AVG PSA Screening" numeric(49,4) DEFAULT NULL,
  "AVG Screening Pelvic Breast" numeric(49,4) DEFAULT NULL,
  "AVG Steroids" numeric(49,4) DEFAULT NULL,
  "AVG Tests for allergens antibodies infectious agents" numeric(49,4) DEFAULT NULL,
  "AVG Tumor Imaging" numeric(49,4) DEFAULT NULL,
  "AVG Ultrasound abdomen / pelvis" numeric(49,4) DEFAULT NULL,
  "AVG Ultrasound extracranial arteries" numeric(49,4) DEFAULT NULL,
  "AVG Ultrasound extremities" numeric(49,4) DEFAULT NULL,
  "AVG Venous thrombosis imaging" numeric(49,4) DEFAULT NULL,
  "AVG X-Ray Spine" numeric(49 4) DEFAULT NULL
) ;




CREATE TABLE "percentiles" (
  "Filter_ID" int  DEFAULT NULL,
  "Episode_ID" varchar(6) DEFAULT NULL,
  "Level" int  DEFAULT NULL,
  "Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
) ;




CREATE TABLE "pmp_su" (
  "pmp_su_id" int   NOT NULL ,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "period" varchar(10) DEFAULT NULL,
  "member_count" int  DEFAULT NULL,
  "total_costs" numeric(40,20) DEFAULT NULL,
  "total_costs_IP" numeric(40,20) DEFAULT NULL,
  "total_costs_OP" numeric(40,20) DEFAULT NULL,
  "total_costs_PB" numeric(40,20) DEFAULT NULL,
  "total_costs_RX" numeric(40,20) DEFAULT NULL,
  "pmp_costs" numeric(40,20) DEFAULT NULL,
  "pmp_costs_IP" numeric(40,20) DEFAULT NULL,
  "pmp_costs_OP" numeric(40,20) DEFAULT NULL,
  "pmp_costs_PB" numeric(40,20) DEFAULT NULL,
  "pmp_costs_RX" numeric(40,20) DEFAULT NULL,
  "member_count_IP_stay" int  DEFAULT NULL,
  "member_count_ED_visit" int  DEFAULT NULL,
  "member_percent_IP_stay" numeric(10,5) DEFAULT NULL,
  "member_percent_ED_visit" numeric(10,5) DEFAULT NULL,
  "member_count_SU" int  DEFAULT NULL,
  "member_count_SU_IP" int  DEFAULT NULL,
  "member_count_SU_ED" int  DEFAULT NULL,
  "member_count_SU_ED_and_IP" int  DEFAULT NULL,
  "member_percent_SU" numeric(10,5) DEFAULT NULL,
  "member_percent_SU_IP" numeric(10,5) DEFAULT NULL,
  "member_percent_SU_ED" numeric(10,5) DEFAULT NULL,
  "member_percent_SU_ED_and_IP" numeric(10,5) DEFAULT NULL,
  "costs_ED_and_IP" numeric(40,20) DEFAULT NULL,
  "costs_SU_ED_or_IP" numeric(40,20) DEFAULT NULL,
  "costs_SU_ED" numeric(40,20) DEFAULT NULL,
  "costs_SU_IP" numeric(40,20) DEFAULT NULL,
  "costs_SU_ED_and_IP" numeric(40,20) DEFAULT NULL,
  "percent_costs_SU_ED_or_IP" numeric(10,5) DEFAULT NULL,
  "percent_ED_costs_SU_ED" numeric(10,5) DEFAULT NULL,
  "percent_IP_costs_SU_IP" numeric(10,5) DEFAULT NULL,
  "percent_costs_SU_ED_and_IP" numeric(10,5) DEFAULT NULL,
  "ED_SU_avg_ED_count" int  DEFAULT NULL,
  "ED_SU_avg_IP_count" int  DEFAULT NULL,
  "ED_SU_avg_ED_LOS" int  DEFAULT NULL,
  "IP_SU_avg_ED_count" int  DEFAULT NULL,
  "IP_SU_avg_IP_count" int  DEFAULT NULL,
  "IP_SU_avg_ED_LOS" int  DEFAULT NULL,
  "ED_and_IP_SU_avg_ED_count" int  DEFAULT NULL,
  "ED_and_IP_SU_avg_IP_count" int  DEFAULT NULL,
  "ED_and_IP_SU_avg_ED_LOS" int  DEFAULT NULL,
  "ED_SU_avg_ED_cost" numeric(15,4) DEFAULT NULL,
  "IP_SU_avg_IP_cost" numeric(15,4) DEFAULT NULL,
  "ED_and_IP_SU_avg_ED_and_IP_cost" numeric(15,4) DEFAULT NULL,
) ;




CREATE TABLE "pmp_su_metric_desc" (
  "pmp_su_metric_id" int   NOT NULL ,
  "metric_name" varchar(30) DEFAULT NULL,
  "metric_desc" varchar(80) DEFAULT NULL 
) ;




CREATE TABLE "pmp_su_ranks" (
  "id" int   NOT NULL ,
  "pmp_su_id" int  DEFAULT NULL,
  "pmp_su_metric_id" int  DEFAULT NULL,
  "rank" int  DEFAULT NULL,
  "count" int  DEFAULT NULL,
  "cost" numeric(40 20) DEFAULT NULL 
) ;




CREATE TABLE "processReport" (
  "uid" int  NOT NULL ,
  "jobUid" int  NOT NULL,
  "stepName" varchar(45) NOT NULL,
  "reportName" varchar(45) DEFAULT NULL,
  "report" VARBINARY(6500),
) ;




CREATE TABLE "provider" (
  "id" int   NOT NULL ,
  "provider_id" varchar(24) DEFAULT NULL,
  "npi" varchar(12) DEFAULT NULL,
  "dea_no" varchar(20) DEFAULT NULL,
  "group_name" varchar(75) DEFAULT NULL,
  "practice_name" varchar(75) DEFAULT NULL,
  "provider_name" varchar(75) DEFAULT NULL,
  "system_name" varchar(75) DEFAULT NULL,
  "tax_id" varchar(11) DEFAULT NULL,
  "medicare_id" varchar(20) DEFAULT NULL,
  "zipcode" varchar(12) DEFAULT NULL,
  "pilot_name" varchar(75) DEFAULT NULL,
  "aco_name" varchar(75) DEFAULT NULL,
  "provider_type" varchar(24) DEFAULT NULL,
  "provider_attribution_code" varchar(24) DEFAULT NULL,
  "provider_aco" varchar(24) DEFAULT NULL,
  "provider_health_system" varchar(75) DEFAULT NULL,
  "provider_medical_group" varchar(75) DEFAULT NULL,
  "facility_id" varchar(30) DEFAULT NULL,
  "member_count" int  DEFAULT NULL,
  "episode_count" int  DEFAULT NULL,
  "severity_score" int  DEFAULT NULL,
  "performance_score" int  DEFAULT NULL 
) ;




CREATE TABLE "provider_attribution" (
  "id" int   NOT NULL ,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "episode_type" varchar(1) DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "trig_begin_date" date DEFAULT NULL,
  "trig_end_date" date DEFAULT NULL,
  "triggering_physician" varchar(30) DEFAULT NULL,
  "triggering_facility" varchar(30) DEFAULT NULL,
  "trig_claim_attr_phys" varchar(30) DEFAULT NULL,
  "trig_claim_attr_fac" varchar(30) DEFAULT NULL,
  "em_count_attr" varchar(30) DEFAULT NULL,
  "em_cost_attr" varchar(30) DEFAULT NULL 
) ;




CREATE TABLE "provider_epid" (
  "id" int   NOT NULL ,
  "provider_id" varchar(30) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "member_count" int  DEFAULT NULL,
  "episode_count" int  DEFAULT NULL,
  "severity_score" int  DEFAULT NULL 
) ;




CREATE TABLE "provider_epid_level" (
  "id" int   NOT NULL ,
  "provider_id" varchar(30) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "split" tinyint  DEFAULT '0',
  "annualized" tinyint  DEFAULT '0',
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40 20) DEFAULT NULL 
) ;




CREATE TABLE "provider_master_epid_level" (
  "id" int   NOT NULL ,
  "provider_id" varchar(24) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "level" tinyint  DEFAULT NULL,
  "split" tinyint  DEFAULT '0',
  "annualized" tinyint  DEFAULT '0',
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40 20) DEFAULT NULL 
) ;




CREATE TABLE "provider_specialty" (
  "id" int   NOT NULL ,
  "provider_uid" int  DEFAULT NULL,
  "specialty_id" varchar(24) DEFAULT NULL,
  "code_source" varchar(10) DEFAULT NULL 
) ;




CREATE TABLE "ra_coef_diags" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "dependent_var"VARCHAR(65000),
  "model_type"VARCHAR(65000),
  "parameter"VARCHAR(65000),
  "coef" double precision precision precision DEFAULT NULL,
  "t_stat" double precision precision precision DEFAULT NULL,
  "p_value" double precision precision precision DEFAULT NULL,
  "vif" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "ra_coeffs" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "parameter"VARCHAR(65000),
  "eol"VARCHAR(65000),
  "coef_ra_typ_ip_l1_use"VARCHAR(65000),
  "coef_ra_typ_ip_l1_cost"VARCHAR(65000),
  "coef_sa_typ_ip_l1_use"VARCHAR(65000),
  "coef_sa_typ_ip_l1_cost"VARCHAR(65000),
  "coef_ra_typ_other_l1_use"VARCHAR(65000),
  "coef_ra_typ_other_l1_cost"VARCHAR(65000),
  "coef_sa_typ_other_l1_use"VARCHAR(65000),
  "coef_sa_typ_other_l1_cost"VARCHAR(65000),
  "coef_ra_comp_other_l1_use"VARCHAR(65000),
  "coef_ra_comp_other_l1_cost"VARCHAR(65000),
  "coef_sa_comp_other_l1_use"VARCHAR(65000),
  "coef_sa_comp_other_l1_cost"VARCHAR(65000),
  "coef_ra_typ_ip_l4_use"VARCHAR(65000),
  "coef_ra_typ_ip_l4_cost"VARCHAR(65000),
  "coef_sa_typ_ip_l4_use"VARCHAR(65000),
  "coef_sa_typ_ip_l4_cost"VARCHAR(65000),
  "coef_ra_typ_other_l4_use"VARCHAR(65000),
  "coef_ra_typ_other_l4_cost"VARCHAR(65000),
  "coef_sa_typ_other_l4_use"VARCHAR(65000),
  "coef_sa_typ_other_l4_cost"VARCHAR(65000),
  "coef_ra_comp_other_l4_use"VARCHAR(65000),
  "coef_ra_comp_other_l4_cost"VARCHAR(65000),
  "coef_sa_comp_other_l4_use"VARCHAR(65000),
  "coef_sa_comp_other_l4_cost"VARCHAR(65000),
  "coef_ra_typ_l1_use"VARCHAR(65000),
  "coef_ra_typ_l1_cost"VARCHAR(65000),
  "coef_sa_typ_l1_use"VARCHAR(65000),
  "coef_sa_typ_l1_cost"VARCHAR(65000),
  "coef_ra_comp_l1_use"VARCHAR(65000),
  "coef_ra_comp_l1_cost"VARCHAR(65000),
  "coef_sa_comp_l1_use"VARCHAR(65000),
  "coef_sa_comp_l1_cost"VARCHAR(65000),
  "coef_ra_typ_l5_use"VARCHAR(65000),
  "coef_ra_typ_l5_cost"VARCHAR(65000),
  "coef_sa_typ_l5_use"VARCHAR(65000),
  "coef_sa_typ_l5_cost"VARCHAR(65000),
  "coef_ra_comp_l5_use"VARCHAR(65000),
  "coef_ra_comp_l5_cost"VARCHAR(65000),
  "coef_sa_comp_l5_use"VARCHAR(65000),
  "coef_sa_comp_l5_cost"VARCHAR(65000),
  "coef_ra_typ_ip_l3_use"VARCHAR(65000),
  "coef_ra_typ_ip_l3_cost"VARCHAR(65000),
  "coef_sa_typ_ip_l3_use"VARCHAR(65000),
  "coef_sa_typ_ip_l3_cost"VARCHAR(65000),
  "coef_ra_typ_other_l3_use"VARCHAR(65000),
  "coef_ra_typ_other_l3_cost"VARCHAR(65000),
  "coef_sa_typ_other_l3_use"VARCHAR(65000),
  "coef_sa_typ_other_l3_cost"VARCHAR(65000),
  "coef_ra_comp_other_l3_use"VARCHAR(65000),
  "coef_ra_comp_other_l3_cost"VARCHAR(65000),
  "coef_sa_comp_other_l3_use"VARCHAR(65000),
  "coef_sa_comp_other_l3_cost"VARCHAR(65000)
) ;




CREATE TABLE "ra_cost_use" (
  "epi_id" varchar(75) DEFAULT NULL,
  "epi_number" varchar(6) DEFAULT NULL,
  "name" varchar(25) DEFAULT NULL,
  "value" int  DEFAULT NULL
) ;




CREATE TABLE "ra_episode_data" (
  "epi_id" varchar(73) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "epi_number" varchar(6) DEFAULT NULL,
  "epi_name" varchar(6) DEFAULT NULL,
  "female" int  DEFAULT NULL,
  "age" int  DEFAULT NULL,
  "rec_enr" int  DEFAULT NULL,
  "eol_ind" int  DEFAULT NULL,
) ;




CREATE TABLE "ra_exp_cost" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "epi_id"VARCHAR(65000),
  "eol_prob" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l1" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l1" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l4" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l5" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l5" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l3" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l3" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l3" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "ra_expected" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "epi_id"VARCHAR(65000),
  "eol_prob" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l4" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l4" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l1" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l1" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l4" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l4" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_l1" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_l1" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_l5" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_l5" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_l5" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_l5" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l5" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l5" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_comp_other_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_comp_other_l3" double precision precision precision DEFAULT NULL,
  "total_exp_cost_ra_l3" double precision precision precision DEFAULT NULL,
  "total_exp_cost_sa_l3" double precision precision precision DEFAULT NULL,
  "use_prob_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_ra_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "use_prob_sa_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "cost_pred_sa_typ_ip_l3" double precision precision precision DEFAULT NULL,
  "exp_cost_sa_typ_ip_l3" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "ra_model_diags" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "dependent_var"VARCHAR(65000),
  "model_type"VARCHAR(65000),
  "r_squared" double precision precision precision DEFAULT NULL,
  "f_stat" double precision precision precision DEFAULT NULL,
  "p_value" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "ra_obs_diags" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "dependent_var"VARCHAR(65000),
  "model_type"VARCHAR(65000),
  "obs" int  DEFAULT NULL,
  "member_id"VARCHAR(65000),
  "cooks_distance" double precision precision precision DEFAULT NULL,
  "fitted" double precision precision precision DEFAULT NULL,
  "resid" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "ra_riskfactors" (
  "epi_id" varchar(75) DEFAULT NULL,
  "epi_number" varchar(6) DEFAULT NULL,
  "name" varchar(25) DEFAULT NULL,
  "value" int  DEFAULT NULL
) ;




CREATE TABLE "ra_riskfactors_codes" (
  "epi_id" varchar(73) DEFAULT NULL,
  "epi_number" varchar(10) DEFAULT NULL,
  "type_id" varchar(3) DEFAULT NULL,
  "code_value" varchar(12) DEFAULT NULL,
  "name" varchar(25) DEFAULT NULL,
  "value" varchar(1) NOT NULL DEFAULT '',
) ;




CREATE TABLE "ra_subtypes" (
  "epi_id" varchar(75) DEFAULT NULL,
  "epi_number" varchar(6) DEFAULT NULL,
  "name" varchar(25) DEFAULT NULL,
  "value" int  DEFAULT NULL
) ;




CREATE TABLE "ra_use_diags" (
  "row_names"VARCHAR(65000),
  "epi_number"VARCHAR(65000),
  "epi_name"VARCHAR(65000),
  "dependent_var"VARCHAR(65000),
  "model_type"VARCHAR(65000),
  "obs" int  DEFAULT NULL,
  "use_prob" double precision precision precision DEFAULT NULL
) ;




CREATE TABLE "RED_ALL_LEVELS" (
  "Filter_ID" int  DEFAULT NULL,
  "Member_ID" varchar(50) DEFAULT NULL,
  "Master_Episode_ID" varchar(255) DEFAULT NULL,
  "Episode_ID" varchar(6) DEFAULT NULL,
  "Episode_Name" varchar(6) DEFAULT NULL,
  "Episode_Description" varchar(255) DEFAULT NULL,
  "Episode_Type" varchar(50) DEFAULT NULL,
  "MDC" int  DEFAULT NULL,
  "MDC_Description" varchar(255) DEFAULT NULL,
  "Episode_Begin_Date" varchar(10) DEFAULT NULL,
  "Episode_End_Date" varchar(10) DEFAULT NULL,
  "Episode_Length" int  DEFAULT NULL,
  "Level" int  DEFAULT NULL,
  "Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Facility_ID" varchar(50) DEFAULT NULL,
  "Physician_ID" varchar(50) DEFAULT NULL,
  "Physician_Specialty" varchar(2) DEFAULT NULL,
  "Split_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "IP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "OP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "PB_PAC_Count" numeric(13,2) DEFAULT NULL,
  "RX_PAC_Count" numeric(13,2) DEFAULT NULL,
  "LEVEL_1" varchar(73) DEFAULT NULL,
  "LEVEL_2" varchar(73) DEFAULT NULL,
  "LEVEL_3" varchar(73) DEFAULT NULL,
  "LEVEL_4" varchar(73) DEFAULT NULL,
  "LEVEL_5" varchar(73) DEFAULT NULL
) ;




CREATE TABLE "report_episode_detail" (
  "Filter_ID" int  DEFAULT NULL,
  "Member_ID" varchar(50) DEFAULT NULL,
  "Master_Episode_ID" varchar(255) DEFAULT NULL,
  "Episode_ID" varchar(6) DEFAULT NULL,
  "Episode_Name" varchar(6) DEFAULT NULL,
  "Episode_Description" varchar(255) DEFAULT NULL,
  "Episode_Type" varchar(50) DEFAULT NULL,
  "MDC" int  DEFAULT NULL,
  "MDC_Description" varchar(255) DEFAULT NULL,
  "Episode_Begin_Date" varchar(10) DEFAULT NULL,
  "Episode_End_Date" varchar(10) DEFAULT NULL,
  "Episode_Length" int  DEFAULT NULL,
  "Level" int  DEFAULT NULL,
  "Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_80thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Facility_ID" varchar(50) DEFAULT NULL,
  "Physician_ID" varchar(50) DEFAULT NULL,
  "Physician_Specialty" varchar(2) DEFAULT NULL,
  "Split_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_IP_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_Typical_Other_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Expected_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "IP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "OP_PAC_Count" numeric(13,2) DEFAULT NULL,
  "PB_PAC_Count" numeric(13,2) DEFAULT NULL,
  "RX_PAC_Count" numeric(13,2) DEFAULT NULL,
) ;




CREATE TABLE "report_episode_summary" (
  "Filter_ID" int  DEFAULT NULL,
  "Episode_ID" varchar(6) DEFAULT NULL,
  "Episode_Name" varchar(6) DEFAULT NULL,
  "Episode_Description" varchar(255) DEFAULT NULL,
  "Episode_Type" varchar(50) DEFAULT NULL,
  "MDC" int  DEFAULT NULL,
  "MDC_Description" varchar(255) DEFAULT NULL,
  "Level" int  DEFAULT NULL,
  "Episode_Volume" int  DEFAULT NULL,
  "Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Min_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Max_Cost" numeric(13,2) DEFAULT NULL,
  "Split_STDEV" numeric(13,2) DEFAULT NULL,
  "Split_CV" numeric(13,2) DEFAULT NULL,
  "Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Average_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_PAC_Percent" numeric(5,2) DEFAULT NULL,
  "Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Average_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Min_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Max_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_STDEV" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_CV" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Average_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_PAC_Percent" numeric(5,2) DEFAULT NULL,
  "Annualized_Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Average_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Min_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Max_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_STDEV" numeric(13,2) DEFAULT NULL,
  "Unsplit_CV" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Average_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_PAC_Percent" numeric(5,2) DEFAULT NULL,
  "Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Unsplit_Average_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_1stPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_99thPercentile_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Min_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Max_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_STDEV" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_CV" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Average_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_PAC_Percent" numeric(5,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Total_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Unsplit_Average_TypicalwPAC_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Split_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Split_Typical_IP_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Split_Typical_Other_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Split_PAC_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Unsplit_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Unsplit_Typical_IP_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Unsplit_Typical_Other_Average_Cost" numeric(13,2) DEFAULT NULL,
  "Expected_Unsplit_PAC_Average_Cost" numeric(13 2) DEFAULT NULL 
) ;




CREATE TABLE "risk_factors" (
  "dx" varchar(25) DEFAULT NULL,
  "type_id" varchar(3) DEFAULT NULL,
  "factor_id" varchar(25) DEFAULT NULL,
  "factorname" varchar(255) DEFAULT NULL 
)  ;




CREATE TABLE "risk_factors_w_subtype_names" (
  "episode_id" varchar(6)  NOT NULL,
  "type_id" varchar(3) DEFAULT NULL,
  "dx" varchar(25) DEFAULT NULL,
  "factor_id" varchar(25) DEFAULT NULL,
  "factorname" varchar(255) DEFAULT NULL,
) ;




CREATE TABLE "run" (
  "uid" int  NOT NULL ,
  "submitter_name" varchar(75) DEFAULT NULL,
  "submitted_date" date DEFAULT NULL,
  "run_name" varchar(75) DEFAULT NULL,
  "data_start_date" date DEFAULT NULL,
  "data_end_date" date DEFAULT NULL,
  "data_latest_begin_date" date DEFAULT NULL,
  "enrolled_population" int  DEFAULT NULL,
  "run_date" date DEFAULT NULL,
  "data_approved_date" date DEFAULT NULL,
  "metadata_version" varchar(100) DEFAULT NULL,
  "metadata_custom" tinyint  DEFAULT '0',
  "episode_count" int  DEFAULT NULL,
  "episode_cost" numeric(15,4) DEFAULT NULL,
  "unassigned_cost" numeric(15,4) DEFAULT NULL,
  "jobUid" int  DEFAULT NULL,
) ;




CREATE TABLE "savings_calc_epis" (
  "filter_id" int  DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "master_episode_id" varchar(255) DEFAULT NULL,
  "episode_id" varchar(6) DEFAULT NULL,
  "episode_name" varchar(6) DEFAULT NULL,
  "episode_type" varchar(50) DEFAULT NULL,
  "MDC" int  DEFAULT NULL,
  "mdc_description" varchar(255) DEFAULT NULL,
  "level" int  DEFAULT NULL,
  "episode_begin_date" varchar(10) DEFAULT NULL,
  "episode_end_date" varchar(10) DEFAULT NULL,
  "Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_PAC_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Total_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Split_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Annualized_Split_Average_Typical_Cost" numeric(13,2) DEFAULT NULL,
  "Potential_Typical_Savings" numeric(14,2) DEFAULT NULL,
  "Potential_PAC_Savings" numeric(17,6) DEFAULT NULL,
  "level_1" varchar(73) DEFAULT NULL,
  "level_2" varchar(73) DEFAULT NULL,
  "level_3" varchar(73) DEFAULT NULL,
  "level_4" varchar(73) DEFAULT NULL,
  "level_5" varchar(73) DEFAULT NULL
) ;




CREATE TABLE "sub_distinct" (
  "child_master_episode_id" varchar(73) DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
) ;




CREATE TABLE "sub_distinct_filter" (
  "child_master_episode_id" varchar(73) DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
) ;




CREATE TABLE "sub_type_codes" (
  "EPISODE_ID" varchar(12) DEFAULT NULL,
  "EPISODE_NAME" varchar(50) DEFAULT NULL,
  "CODE_VALUE" varchar(25) DEFAULT NULL,
  "TYPE_ID" varchar(10) DEFAULT NULL,
  "GROUP_ID" varchar(12) DEFAULT NULL,
  "GROUP_NAME" varchar(255) DEFAULT NULL,
  "SUBTYPE_GROUP_ID" varchar(25) DEFAULT NULL
) ;




CREATE TABLE "sub_type_codes_consolidated" (
  "EPISODE_ID" varchar(12) DEFAULT NULL,
  "EPISODE_NAME" varchar(50) DEFAULT NULL,
  "CODE_VALUE" varchar(25) DEFAULT NULL,
  "TYPE_ID" varchar(10) DEFAULT NULL,
  "GROUP_ID" varchar(12) DEFAULT NULL,
  "GROUP_NAME" varchar(255) DEFAULT NULL,
  "SUBTYPE_GROUP_ID" varchar(25) DEFAULT NULL,
) ;




CREATE TABLE "tmp_ann_mel" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "level" int  DEFAULT NULL,
  "parent_child_id" varchar(2) DEFAULT NULL,
  "cost" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_t" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_tc" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_c" numeric(40 20) DEFAULT '0.00000000000000000000' 
) ;




CREATE TABLE "tmp_annids" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "parent_child_id" varchar(2) DEFAULT NULL 
) ;




CREATE TABLE "tmp_annids_c" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "lvl" int  DEFAULT NULL 
) ;




CREATE TABLE "tmp_enroll_gap" (
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "episode_length" int  DEFAULT NULL,
  "gap_length" numeric(35,0) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL
) ;




CREATE TABLE "tmp_enroll_gap1" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "member_id" varchar(50) DEFAULT NULL,
  "episode_length" int  DEFAULT NULL,
  "gap_length" numeric(35,0) DEFAULT NULL,
  "filter_fail" int  DEFAULT NULL
) ;




CREATE TABLE "tmp_filt_proc_orp_trig" (
  "master_episode_id" varchar(73) DEFAULT NULL 
) ;




CREATE TABLE "tmp_hlow" (
  "episode_id" varchar(10) DEFAULT NULL,
  "low" numeric(40,20) DEFAULT NULL,
  "high" numeric(40,20) DEFAULT NULL,
) ;




CREATE TABLE "tmp_mel1" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "cost" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_t" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_tc" numeric(40,20) DEFAULT '0.00000000000000000000',
  "cost_c" numeric(40 20) DEFAULT '0.00000000000000000000' 
) ;




CREATE TABLE "tmp_proc_orph" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "begin_date" date DEFAULT NULL,
  "end_date" date DEFAULT NULL,
  "confirmed" int  DEFAULT '0' 
) ;




CREATE TABLE "tmp_prov_attr" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "physician_id" varchar(24) DEFAULT NULL,
  "max_count" int  NOT NULL DEFAULT '0',
  "max_cost" int  NOT NULL DEFAULT '0' 
) ;




CREATE TABLE "tmp_provider_attribution_em" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "physician_id" varchar(24) DEFAULT NULL,
  "phys_count" int  NOT NULL DEFAULT '0',
  "phys_cost" numeric(62,20) DEFAULT NULL,
  "max_count" binary(1) DEFAULT NULL,
  "max_cost" binary(1) DEFAULT NULL 
) ;




CREATE TABLE "tmp_sub_association" (
  "uber_master_episode_id" varchar(73) DEFAULT NULL,
  "parent_master_episode_id" varchar(73) DEFAULT NULL,
  "child_master_episode_id" varchar(73) DEFAULT NULL,
  "association_type" varchar(24) DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
  "association_count" int  DEFAULT NULL,
  "association_sub_level" tinyint  DEFAULT NULL,
  "cost" numeric(40,20) DEFAULT NULL,
  "cost_t" numeric(40,20) DEFAULT NULL,
  "cost_tc" numeric(40,20) DEFAULT NULL,
  "cost_c" numeric(40,20) DEFAULT NULL,
  "association_sub_level_inv" tinyint  DEFAULT NULL 
) ;




CREATE TABLE "tmp_subt_claims" (
  "master_claim_id" varchar(100) DEFAULT NULL,
  "id" int  DEFAULT NULL,
  "epi" varchar(10) DEFAULT NULL,
  "trig" varchar(10) DEFAULT NULL,
  "begin_date" datetime DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
) ;




CREATE TABLE "tmp_total_cost" (
  "level" int  DEFAULT NULL,
  "total_cost_unsplit" numeric(40,20) DEFAULT NULL,
  "total_cost_split" numeric(40 20) DEFAULT NULL
) ;




CREATE TABLE "tmp_trig_claims" (
  "master_episode_id" varchar(73) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "episode_type" varchar(1) DEFAULT NULL,
  "assigned_type" varchar(2) DEFAULT NULL,
  "claim_line_type_code" varchar(12) DEFAULT NULL,
  "allowed_amt" numeric(15,4) DEFAULT NULL,
  "split_allowed_amt" numeric(15,4) DEFAULT NULL,
) ;




CREATE TABLE "tmp_uber" (
  "id" int   NOT NULL ,
  "parent_master_episode_id" varchar(73) DEFAULT NULL,
  "child_master_episode_id" varchar(73) DEFAULT NULL,
  "association_type" varchar(24) DEFAULT NULL,
  "association_level" tinyint  DEFAULT NULL,
  "association_count" int  DEFAULT NULL,
  "association_start_day" varchar(15) DEFAULT NULL,
  "association_end_day" varchar(15) DEFAULT NULL,
) ;




CREATE TABLE "triggers" (
  "id" int   NOT NULL ,
  "member_id" varchar(50) DEFAULT NULL,
  "claim_id" varchar(50) DEFAULT NULL,
  "claim_line_id" varchar(22) DEFAULT NULL,
  "master_episode_id" varchar(73) DEFAULT NULL,
  "master_claim_id" varchar(100) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "episode_type" varchar(1) DEFAULT NULL,
  "trig_begin_date" date DEFAULT NULL,
  "trig_end_date" date DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "episode_end_date" date DEFAULT NULL,
  "orig_episode_begin_date" date DEFAULT NULL,
  "orig_episode_end_date" date DEFAULT NULL,
  "look_back" varchar(4) DEFAULT NULL,
  "look_ahead" varchar(4) DEFAULT NULL,
  "req_conf_claim" tinyint  DEFAULT '0',
  "conf_claim_id" varchar(50) DEFAULT NULL,
  "conf_claim_line_id" varchar(10) DEFAULT NULL,
  "min_code_separation" int  DEFAULT NULL,
  "max_code_separation" varchar(6) DEFAULT NULL,
  "trig_by_episode_id" varchar(10) DEFAULT NULL,
  "trig_by_master_episode_id" varchar(73) DEFAULT NULL,
  "dx_pass_code" varchar(12) DEFAULT NULL,
  "px_pass_code" varchar(12) DEFAULT NULL,
  "em_pass_code" varchar(12) DEFAULT NULL,
  "conf_dx_pass_code" varchar(12) DEFAULT NULL,
  "conf_px_pass_code" varchar(12) DEFAULT NULL,
  "conf_em_pass_code" varchar(12) DEFAULT NULL,
  "dropped" tinyint  DEFAULT '0',
  "truncated" tinyint  DEFAULT '0',
  "win_claim_id" varchar(50) DEFAULT NULL,
  "win_master_claim_id" varchar(73) DEFAULT NULL 
) ;




CREATE TABLE "unassigned_episodes" (
  "member_id" varchar(50) DEFAULT NULL,
  "episode_id" varchar(10) DEFAULT NULL,
  "episode_begin_date" date DEFAULT NULL,
  "cost" numeric(40 20) DEFAULT NULL 
) ;








